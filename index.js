function testService(events) {
    if(!events.length){return true}
    let Visitors = {in: 0, out: 0}
    for (let i = 0; i < events.length; i++) {
        const name = events[i][0]
        const status = events[i][1]
        if(status ==='in'){
            Visitors.in +=1
            if(Visitors[name] === 'in'){
                return false //Посетитель зашел несколько раз
            }
            if(!Visitors[name]){
                Visitors[name]= status
            }
        }
        if(status === 'out'){
            Visitors.out +=1
            if(Visitors[name] === 0){
                return false //Посетитель вышел несколько раз
            }
            if(Visitors[name] === 'in'){
                Visitors[name] = 0
            }
            if(Visitors[name] === undefined){
                return false //Вышел неизвестный посетитель
            }
        }
    }
    return Visitors.in === Visitors.out
}
module.exports = testService
